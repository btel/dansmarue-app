import React, { useRef, useEffect, useState } from 'react';
import mapboxgl from 'mapbox-gl';
import geoJson from "./test.json";

import './Map.css';

mapboxgl.accessToken = process.env.REACT_APP_MAPBOX_API_TOKEN 
  

// Longitude: 2.3287 | Latitude: 48.8306 | Zoom: 13.64
const Map = () => {
  const mapContainerRef = useRef(null);

  const [lng, setLng] = useState(2.3287);
  const [lat, setLat] = useState(48.8306);
  const [zoom, setZoom] = useState(13);

  // Initialize map when component mounts
  useEffect(() => {
    const map = new mapboxgl.Map({
      container: mapContainerRef.current,
      style: 'mapbox://styles/btel/clmkj8zef01s601r4d1mgdwzy',
      center: [lng, lat],
      zoom: zoom
    });

    map.on('click', 'trees-point', function (e) {
      var coordinates = e.features[0].geometry.coordinates.slice();
      var address = e.features[0].properties.adresse;
      var datedecl = e.features[0].properties.datedecl;
      var streetview = "<a href='https://www.google.com/maps/@?api=1&map_action=pano&viewpoint=" + coordinates[1] + "," + coordinates[0] + "' target='_blank'>Google Street View</a>";
      var description = "<h3>Adresse: " + address + "</h3><p>Date de déclaration: " + datedecl + "</p>" + streetview;

      // add a link to google street view
      // make a popup for each feature and add to the map
      new mapboxgl.Popup()
      .setLngLat(coordinates)
      .setHTML(description)
      .addTo(map);
      });

    map.on("load", function () {
    
    map.addSource("points", {
      type: "geojson",
      data: {
        type: "FeatureCollection",
        features: geoJson.features,
      },
    });

    map.addLayer(
    {
    'id': 'incidents-heat',
    'type': 'heatmap',
    'source': 'points',
    'maxzoom': 17,
    'paint': {
    // Increase the heatmap weight based on frequency and property magnitude
    'heatmap-weight': 0.05,
    // Increase the heatmap color weight weight by zoom level
    // heatmap-intensity is a multiplier on top of heatmap-weight
    'heatmap-intensity': [
    'interpolate',
    ['linear'],
    ['zoom'],
    0,
    1,
    17,
    3
    ],
    // Color ramp for heatmap.  Domain is 0 (low) to 1 (high).
    // Begin color ramp at 0-stop with a 0-transparancy color
    // to create a blur-like effect.
    'heatmap-color': [
    'interpolate',
    ['linear'],
    ['heatmap-density'],
    0,
    'rgba(33,102,172,0)',
    0.2,
    'rgb(103,169,207)',
    0.4,
    'rgb(209,229,240)',
    0.6,
    'rgb(253,219,199)',
    0.8,
    'rgb(239,138,98)',
    1,
    'rgb(178,24,43)'
    ],
    // Adjust the heatmap radius by zoom level
    'heatmap-radius': [
    'interpolate',
    ['linear'],
    ['zoom'],
    0,
    4,
    17,
    12
    ],
    // Transition from heatmap to circle layer by zoom level
    'heatmap-opacity': [
    'interpolate',
    ['linear'],
    ['zoom'],
    12,
    1,
    17,
    0
    ]
    }
    },
    'waterway-label'
    );

map.addLayer(
  {
    id: 'trees-point',
    type: 'circle',
    source: 'points',
    minzoom: 14,
    paint: {
      // increase the radius of the circle as the zoom level and dbh value increases
      'circle-radius': {
        property: 'dbh',
        type: 'exponential',
        stops: [
          [{ zoom: 15, value: 1 }, 5],
          [{ zoom: 15, value: 62 }, 10],
          [{ zoom: 22, value: 1 }, 20],
          [{ zoom: 22, value: 62 }, 50]
        ]
      },
      'circle-color': {
        property: 'dbh',
        type: 'exponential',
        stops: [
          [0, 'rgba(236,222,239,0)'],
          [10, 'rgb(236,222,239)'],
          [20, 'rgb(208,209,230)'],
          [30, 'rgb(166,189,219)'],
          [40, 'rgb(103,169,207)'],
          [50, 'rgb(28,144,153)'],
          [60, 'rgb(1,108,89)']
        ]
      },
      'circle-stroke-color': 'white',
      'circle-stroke-width': 1,
      'circle-opacity': {
        stops: [
          [14, 0],
          [15, 1]
        ]
      }
    }
  },
  'waterway-label'
);

   });

    // Add navigation control (the +/- zoom buttons)
    map.addControl(new mapboxgl.NavigationControl(), 'top-right');

    map.on('move', () => {
      setLng(map.getCenter().lng.toFixed(4));
      setLat(map.getCenter().lat.toFixed(4));
      setZoom(map.getZoom().toFixed(2));
    });

    // Clean up on unmount
    return () => map.remove();
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  return (
    <div>
      <div className='sidebarStyle'>
        <div>
          Dans Ma Rue - Longitude: {lng} | Latitude: {lat} | Zoom: {zoom}
        </div>
      </div>
      <div className='map-container' ref={mapContainerRef} />
    </div>
  );
};

export default Map;
