import React from 'react';
import Map from './Map';

function App() {
  return (
    <div>
      <h1> Dans Ma Rue: Stationnement gênant </h1>
      <Map />
    </div>
  );
}

export default App;
